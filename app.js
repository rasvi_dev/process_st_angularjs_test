angular.module('app', [], function() {})
FileUploadCtrl.$inject = ['$scope']
 
var api_password="c668f7f74e4f6ba1dcc6d0e92c46997ac1e979a4ba5c07f2465885c72446e314";

function FileUploadCtrl(scope) {


    scope.deleteVideo=function(Hashid, id, project_id){
       
       if (confirm("Are you sure? delete"))
           {
        var fd = new FormData()
        fd.append("api_password", api_password)
       
      
        var xhr = new XMLHttpRequest()
        //xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", function(evt){
          
          var getData=JSON.parse(evt.srcElement.response);
          alert(evt.srcElement.response);


          //console.log(getData[0].hashedId);
        }, false)
       
        var url="https://api.wistia.com/v1/medias/"+Hashid+".json?api_password="+api_password

        xhr.open("DELETE",url )
        xhr.send(fd)
      }
    }
 

    scope.setFiles = function(element) {
    scope.$apply(function(scope) {
      console.log('files:', element.files);
      // Turn the FileList object into an Array
        scope.files = []
        for (var i = 0; i < element.files.length; i++) {
          scope.files.push(element.files[i])
        }
      scope.progressVisible = false
      });
    };

   

    scope.uploadFile = function() {
        var fd = new FormData()
        for (var i in scope.files) {
            fd.append("uploadedFile", scope.files[i])
            fd.append("api_password", api_password);
        }
        var xhr = new XMLHttpRequest()
        xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", uploadComplete, false)
        xhr.addEventListener("error", uploadFailed, false)
        xhr.addEventListener("abort", uploadCanceled, false)
        xhr.open("POST", "https://upload.wistia.com")
        scope.progressVisible = true
        xhr.send(fd)
    }


    function  getHashIdProjects(){

        var fd = new FormData()
        fd.append("api_password", api_password)
       
      
        var xhr = new XMLHttpRequest()
        //xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", function(evt){
          var getData=JSON.parse(evt.srcElement.response);
          //console.log(getData[0].hashedId);
          getProjectLists(getData[0].hashedId, getData[0].publicId);
          
        }, false)
       
        xhr.open("GET", "https://api.wistia.com/v1/projects.json?api_password="+api_password)
        xhr.send(fd)


    }


    getHashIdProjects()
    
    scope.projectList=[];

    function getProjectLists(hashId, projectId){

        var url = 'https://api.wistia.com/v1/projects/'+hashId+'.json?api_password='+api_password

        var fd = new FormData()
        fd.append("api_password", api_password)
       
      
        var xhr = new XMLHttpRequest()
        //xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", function(evt){
          var getData=JSON.parse(evt.srcElement.response);
         // console.log(getData.medias);
           scope.$apply(function(scope) {
          
          scope.projectList=getData.medias;
          scope.projectId=projectId

        })

        }, false)
       
        xhr.open("GET", url)
        xhr.send(fd)


    }

       




    function uploadProgress(evt) {
        scope.$apply(function(){
            if (evt.lengthComputable) {
                scope.progress = Math.round(evt.loaded * 100 / evt.total)
            } else {
                scope.progress = 'unable to compute'
            }
        })
    }

    function uploadComplete(evt) {
        /* This event is raised when the server send back a response */
       alert(evt.srcElement.response);
      
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.")
    }

    function uploadCanceled(evt) {
        scope.$apply(function(){
            scope.progressVisible = false
        })
        
    }




}

